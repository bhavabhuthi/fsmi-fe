# FSMI website Front-End

A front end for FSMI website using the Drupal Content API.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yml
image: node

before_script:
  - npm install

cache:
  paths:
    - node_modules/

pages:
  script:
    - npm run generate
  artifacts:
    paths:
      - public
  only:
    - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Nuxt
1. Generate and preview the website with hot-reloading: `npm run dev` or `nuxt`
1. Make respective changes

[ci]: https://about.gitlab.com/gitlab-ci/
[Nuxt]: https://nuxtjs.org/
[install]: https://nuxtjs.org/guide/installation/
[documentation]: https://nuxtjs.org/guide
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

----

Created using https://github.com/nuxt/create-nuxt-app
